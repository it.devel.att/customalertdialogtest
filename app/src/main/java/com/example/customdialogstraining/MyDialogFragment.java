package com.example.customdialogstraining;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.appcompat.widget.AppCompatButton;
import androidx.core.content.ContextCompat;

import java.util.Random;

public class MyDialogFragment extends AppCompatDialogFragment {
    private Context context;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.context = null;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        return super.onCreateDialog(savedInstanceState);
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Важное сообщение")
//                .setMessage("Покормите кота!")
//                .setIcon(R.drawable.ic_launcher_foreground)
//                .setPositiveButton("Ок, иду на кухню", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialog, int which) {
//                        dialog.cancel();
//                    }
//                });
//        return builder.create();


//        String title = "Выбор есть всегда";
//        String message = "Выбери пищу";
//        String button1String = "Вкусная пища";
//        String button2String = "Здоровая пища";
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle(title);
//        builder.setMessage(message);
//        builder.setPositiveButton(button1String, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "Вы сделали правильный выбор", Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.setNegativeButton(button2String, new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Toast.makeText(getActivity(), "Возможно вы правы", Toast.LENGTH_LONG).show();
//            }
//        });
//        builder.setCancelable(false);


//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//        View view = inflater.inflate(R.layout.custom_dialog, null);
//        builder.setView(view);
//        builder.create().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        return builder.create();
//        LayoutInflater inflater = getActivity().getLayoutInflater();
//
//
//        View view = inflater.inflate(R.layout.custom_dialog, null);
//
//        ImageView dialogImage = view.findViewById(R.id.dialogImage);
//        dialogImage.setBackground(ContextCompat.getDrawable(context.getApplicationContext(),R.drawable.ic_idea_black));
//
//        TextView dialogTitle = view.findViewById(R.id.dialogTitle);
//        dialogTitle.setText("Saved!");
//
//        TextView dialogDescription = view.findViewById(R.id.dialogDescription);
//        dialogDescription.setText("Looks like you got a great idea");
//
//        AppCompatButton dialogButton = view.findViewById(R.id.dialogButton);
//        dialogButton.setText("OK. Cool");

        Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(getSuccessOrFailDialogView());
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }


    private View getSuccessOrFailDialogView() {
        Random random = new Random();
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.custom_dialog, null);
        ImageView dialogImage = view.findViewById(R.id.dialogImage);
        TextView dialogTitle = view.findViewById(R.id.dialogTitle);
        TextView dialogDescription = view.findViewById(R.id.dialogDescription);
        AppCompatButton dialogButton = view.findViewById(R.id.dialogButton);

        if (random.nextInt(2) == 1) {
            dialogImage.setBackground(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.ic_idea));
            dialogTitle.setText("Saved!");
            dialogDescription.setText("Looks like you got a great idea");
            dialogButton.setText("OK. Cool");
        } else {
            dialogImage.setBackground(ContextCompat.getDrawable(context.getApplicationContext(), R.drawable.ic_idea_black));
            dialogTitle.setText("Whoops!");
            dialogDescription.setText("Something went wrong!");
            dialogButton.setText("Try Again");
            dialogButton.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorAlertDialogFail));
        }
        return view;
    }
}
